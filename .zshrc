# Created by newuser for 5.6.2
source ~/.antigen.zsh

antigen use oh-my-zsh
antigen bundle git
antigen bundle command-not-found
antigen bundle zsh-users/zsh-syntax-highlighting
antigen theme https://github.com/denysdovhan/spaceship-prompt spaceship
antigen apply

fpath=(/usr/local/share/zsh-completions $fpath)

export PATH="/usr/local/opt/openssl/bin:$PATH"
export LDFLAGS="-L/usr/local/opt/openssl/lib"
export CPPFLAGS="-I/usr/local/opt/openssl/include"

# Set NVM
export NVM_DIR="$HOME/.nvm"
. "/usr/local/opt/nvm/nvm.sh"

# Set ANT_HOME, MAVEN_HOME and GRADLE_HOME
export ANT_HOME=/usr/local/opt/ant
export MAVEN_HOME=/usr/local/opt/maven
export GRADLE_HOME=/usr/local/opt/gradle

# Set ANDROID_SDK_ROOT, ANDROID_NDK_ROOT
export ANDROID_SDK_ROOT="/usr/local/share/android-sdk"
export ANDROID_NDK_HOME="/usr/local/share/android-ndk"

# Some programs will need ANDROID_HOME
export ANDROID_HOME=$ANDROID_SDK_ROOT

#ADD to the path
export PATH=$ANT_HOME/bin:$PATH
export PATH=$MAVEN_HOME/bin:$PATH
export PATH=$GRADLE_HOME/bin:$PATH
export PATH=$ANDROID_SDK_ROOT/tools:$PATH

#ADD the platform-tools and the build tools to the path, the build tools command and sort it by the latest version
export PATH=$ANDROID_SDK_ROOT/platform-tools:$PATH
export PATH=$ANDROID_SDK_ROOT/build-tools/$(ls $ANDROID_SDK_ROOT/build-tools | sort | tail -1):$PATH

# Workaround for the emulator not to give you a QT Lib related error
function emulator { ( cd "$(dirname "$(whence -p emulator)")" && ./emulator "$@"; ) }